/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// https://redux-toolkit.js.org/api/createAsyncThunk
// createAsyncThunk takes two argument :
//   - Name that helps to identify action types
//   - A callback function that should return a promise
// It provides three states : pending, fulfilled and rejected.
export const userLogin = createAsyncThunk(
  'user/login',
  async ({ email, password }: { email: string, password: string }, thunkAPI) => {
    try {
      const response = await fetch(
        'http://localhost:3001/api/v1/user/login',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ email, password }),
        },
      );

      const data = await response.json();

      if (response.status === 200) {
        return data;
      }

      return thunkAPI.rejectWithValue(data);
    } catch (e: any) {
      console.log(e.response.data);
      thunkAPI.rejectWithValue(e.response.data);
    }
  },
);

export const fetchUserBytoken = createAsyncThunk(
  'user/profile',
  async ({ token }: {token: string }, thunkAPI) => {
    try {
      const response = await fetch(
        'http://localhost:3001/api/v1/user/profile',
        {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );

      const data = await response.json();

      if (response.status === 200) {
        return data;
      }

      return thunkAPI.rejectWithValue(data);
    } catch (e: any) {
      console.log(e.response.data);
      thunkAPI.rejectWithValue(e.response.data);
    }
  },
);

export const updateUserProfile = createAsyncThunk(
  'user/profile',
  async (
    { token, firstName, lastName }: {token: string, firstName: string, lastName: string }, thunkAPI,
  ) => {
    try {
      const response = await fetch(
        'http://localhost:3001/api/v1/user/profile',
        {
          method: 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({ firstName, lastName }),
        },
      );

      const data = await response.json();

      if (response.status === 200) {
        return data;
      }

      return thunkAPI.rejectWithValue(data);
    } catch (e: any) {
      console.log(e.response.data);
      thunkAPI.rejectWithValue(e.response.data);
    }
  },
);

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    userFirstName: '',
    userLastName: '',
    userEmail: '',
    isLoggedIn: false,
    isFetching: false,
    isError: false,
    errorMessage: '',
  },
  reducers: {
    clearState: (state) => {
      state.isError = false;
      state.isFetching = false;

      return state;
    },
    resetUserState: (state) => {
      state.userFirstName = '';
      state.userLastName = '';
      state.userEmail = '';
      state.isLoggedIn = false;

      return state;
    },
  },
  extraReducers: {
    [userLogin.fulfilled as any]: (state, { payload }) => {
      state.isFetching = false;
      state.isLoggedIn = true;
      localStorage.setItem('token', payload.body.token);
      return state;
    },
    [userLogin.rejected as any]: (state, { payload }) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = payload.message;
    },
    [userLogin.pending as any]: (state) => {
      state.isFetching = true;
    },
    [fetchUserBytoken.fulfilled as any]: (state, { payload }) => {
      state.userFirstName = payload.body.firstName;
      state.userLastName = payload.body.lastName;
      state.userEmail = payload.body.email;
      state.isFetching = false;
      state.isLoggedIn = true;
      return state;
    },
    [fetchUserBytoken.rejected as any]: (state, { payload }) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = payload.message;
    },
    [fetchUserBytoken.pending as any]: (state) => {
      state.isFetching = true;
    },
    [updateUserProfile.fulfilled as any]: (state, { payload }) => {
      state.userFirstName = payload.body.firstName;
      state.userLastName = payload.body.lastName;
      state.isFetching = false;
      state.isLoggedIn = true;
      return state;
    },
    [updateUserProfile.rejected as any]: (state, { payload }) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = payload.message;
    },
    [updateUserProfile.pending as any]: (state) => {
      state.isFetching = true;
    },
  },
});

export const { clearState, resetUserState } = userSlice.actions;
export default userSlice.reducer;
