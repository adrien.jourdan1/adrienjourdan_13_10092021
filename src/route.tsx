import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import Home from './pages/home';
import SignIn from './pages/sign-in';
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';
import Profile from './pages/profile';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  max-width: 100vw;
  overflow: hidden;
`;

const Routing: React.FC = () => (
  <Router>
    <Header />
    <Container>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/sign-in" component={SignIn} />
        <Route path="/profile" component={Profile} />
      </Switch>
    </Container>
    <Footer />
  </Router>
);

export default Routing;
