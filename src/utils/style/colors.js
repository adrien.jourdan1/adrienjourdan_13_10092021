export default {
  primary: '#020203',
  secondary: '#e51c21',
  blue: '#4AB8FF',
  yellow: '#FDCC0C',
  pink: '#FD5181',
  grey: '#282D30',
  background: '#FBFBFB',
};
