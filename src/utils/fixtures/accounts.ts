export enum EAccountType {
  Saving,
  Current,
  Credit,
}

export interface IAccount {
  title: string,
  type: EAccountType,
  lastNumbers: number,
  sold: number,
}

export const accounts: IAccount[] = [
  {
    title: 'Argent Bank Checking',
    lastNumbers: 8349,
    sold: 2082.79,
    type: 1,
  },
  {
    title: 'Argent Bank Savings',
    lastNumbers: 6712,
    sold: 10928.42,
    type: 0,
  },
  {
    title: 'Argent Bank Credit Card',
    lastNumbers: 8349,
    sold: 184.30,
    type: 2,
  },
];
