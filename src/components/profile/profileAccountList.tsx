import React from 'react';
import styled from 'styled-components';
import { accounts } from '../../utils/fixtures/accounts';
import ProfileAccountListItem from './profileAccountListItem';

const Container = styled.div``;

const ProfileAccountList: React.FC = () => (
  <Container>
    {accounts.map((account) => (
      <ProfileAccountListItem key={account.title} account={account} />
    ))}
  </Container>
);

export default ProfileAccountList;
