/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { useForm } from 'react-hook-form';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { AppDispatch } from '../../app/store';
import { updateUserProfile } from '../../features/User/UserSLice';

const Container = styled.dialog`
  position: fixed;
  top: 50%;
  transform: translate(0, -300px);
  background-color: white;
  padding: 45px;

  h2 {
    margin-top: 0;
  }
`;

const InputWrapper = styled.label`
  font-weight: bold;
  display: flex;
  flex-direction: column;
  text-align: left;
  margin-bottom: 1rem;

  input {
    padding: 5px;
    font-size: 1.2rem;
  }
`;

const SubmitBtn = styled.button`
  cursor: pointer;
  width: 100%;
  padding: 8px;
  font-size: 1.1rem;
  font-weight: bold;
  margin-top: 1rem;
  border: none;
  background-color: #00bc77;
  color: #fff;
  text-decoration: underline;

  &:hover {
    background-color: #00b170;
  }

  &:disabled {
    cursor: initial;
    color: rgba(0,0,0,.26);
    background-color: #dcdcdc;
  }
`;

const CloseBtn = styled.button`
  cursor: pointer;
  width: 100%;
  padding: 8px;
  font-size: 1.1rem;
  font-weight: bold;
  margin-top: 1rem;
  border: none;
  background-color: #bd3b14;
  color: #fff;
  text-decoration: underline;

  &:hover {
    background-color: #b1330d;
  }

  &:disabled {
    cursor: initial;
    color: rgba(0,0,0,.26);
    background-color: #dcdcdc;
  }
`;

interface IProp {
  open: boolean,
  updateModal: any,
}

const ProfileEditModal: React.FC<IProp> = ({ open, updateModal }) => {
  const dispatch: AppDispatch = useAppDispatch();
  const { handleSubmit, register } = useForm();
  const {
    isFetching, userFirstName, userLastName,
  } = useAppSelector((state) => state.user);

  const onSubmit = (
    data: { firstName: string; lastName: string; },
  ) => {
    const trueData = {
      ...data,
      token: localStorage.getItem('token') as string,
    };

    dispatch(updateUserProfile(trueData));

    if (open) {
      updateModal(false);
    }
  };

  const onClick = () => {
    if (open) {
      updateModal(false);
    }
  };

  return (
    <Container open={open}>
      <h2>Edit Profile</h2>
      <form onSubmit={handleSubmit(onSubmit)} method="POST">
        <InputWrapper htmlFor="firstname">
          Firstname
          <input id="firstname" type="text" defaultValue={userFirstName} {...register('firstName')} required />
        </InputWrapper>
        <InputWrapper htmlFor="lastname">
          Lastname
          <input id="lastname" type="text" defaultValue={userLastName} {...register('lastName')} required />
        </InputWrapper>
        <SubmitBtn type="submit">
          {isFetching ? 'Fetching ...' : 'Update'}
        </SubmitBtn>
        <CloseBtn type="button" onClick={onClick} disabled={isFetching}>
          {isFetching ? 'Fetching ...' : 'Cancel'}
        </CloseBtn>
      </form>
    </Container>
  );
};

export default ProfileEditModal;
