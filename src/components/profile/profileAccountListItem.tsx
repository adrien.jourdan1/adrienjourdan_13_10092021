import React from 'react';
import styled from 'styled-components';
import { EAccountType, IAccount } from '../../utils/fixtures/accounts';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border: 1px solid black;
  background-color: #fff;
  width: 80%;
  margin: 0 auto;
  padding: 1.5rem;
  box-sizing: border-box;
  text-align: left;
  margin-bottom: 2rem;

  h3 {
    margin: 0;
    padding: 0;
    font-size: 1rem;
    font-weight: normal;
  }

  p {
    margin: 0;

    strong {
      font-size: 2.5rem;
      font-weight: bold;
    }
  }
`;

const ProfileAccountListItem: React.FC<{account: IAccount}> = ({ account }) => {
  const {
    title, type, lastNumbers, sold,
  } = account;

  return (
    <Container>
      <div>
        <h3>{`${title} (x${lastNumbers})`}</h3>
        <p><strong>{`$${sold}`}</strong></p>
        {
          (type === EAccountType.Current || type === EAccountType.Saving) ? (
            <p>Available Balance</p>
          ) : (
            <p>Current Balance</p>
          )
        }
      </div>
    </Container>
  );
};

export default ProfileAccountListItem;
