import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  flex: 1;
  padding: 2.5rem;
  text-align: center;

  h3 {
    color: #222;
    font-size: 1.25rem;
    font-weight: bold;
    margin-bottom: 0.5rem;
  }

  p {
    color: #2c3e50;
  }
`;

const Icon = styled.img`
  width: 152px;
  border: 10px solid #00bc77;
  border-radius: 50%;
  padding: 1rem;
`;

interface Prop {
  src: string,
  title: string,
  text:string,
  altText:string,
}

const FeaturesItem: React.FC<Prop> = ({
  src, title, text, altText,
}) => (
  <Container>
    <Icon src={src} alt={altText} />
    <h3>{ title }</h3>
    <p>{ text }</p>
  </Container>
);

export default FeaturesItem;
