import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  height: 300px;
  width: 100%;
  position: relative;
  background-image: url('../assets/bank-tree.jpeg');
  background-size: cover;
  background-repeat: no-repeat; 
  background-position: 0 -50px;

  @media (min-width: 920px) {
    height: 400px;
    background-position: 0% 33%;
  }
`;

const HeroContent = styled.section`
  position: relative;
  top: 2rem;
  width: 264px;
  margin: 0 auto;
  background: white;
  padding: 2rem;
  text-align: left;
  color: #2c3e50;

  @media (min-width: 920px) {
    position: absolute;
    right: 50px;
    top: 50px;
    width: 364px;
    margin: 2rem;
  }

  p {
    font-size: 0.9rem;
    margin-bottom: 0;
    

    @media (min-width: 920px) {
      font-size: 1.2rem;
      margin: 0;
      margin-top: 19.2px;
    }
  }

  h3 {
    font-size: 1rem;
    font-weight: bold;
    margin: 0;

    @media (min-width: 920px) {
      font-size: 1.5rem;
    }
  }
`;

const Hero: React.FC = () => (
  <Container>
    <HeroContent>
      <h3>No fees.</h3>
      <h3>No minimum deposit.</h3>
      <h3>High interest rates.</h3>
      <p>Open a savings account with Argent Bank today!</p>
    </HeroContent>
  </Container>
);

export default Hero;
