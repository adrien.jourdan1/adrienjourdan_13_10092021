import React from 'react';
import styled from 'styled-components';
import FeaturesItem from './FeaturesItem';

const Container = styled.section`
  width: 100%;
  display: flex;
  flex-direction: column;

  @media (min-width: 920px) {
    flex-direction: row;
  }
`;

const Features: React.FC = () => (
  <Container>
    <FeaturesItem
      src="./assets/icon-chat.png"
      altText="Chat Icon"
      title="You are our #1 priority"
      text="Need to talk to a representative? You can get in touch through our 24/7 chat or through a phone call in less than 5 minutes."
    />
    <FeaturesItem
      src="./assets/icon-money.png"
      altText="Money Icon"
      title="More savings means higher rates"
      text="The more you save with us, the higher your interest rate will be! "
    />
    <FeaturesItem
      src="./assets/icon-security.png"
      altText="Security Icon"
      title="Security you can trust"
      text="We use top of the line encryption to make sure your data and money is always safe."
    />
  </Container>
);

export default Features;
