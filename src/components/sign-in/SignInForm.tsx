/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';
import { clearState, userLogin } from '../../features/User/UserSLice';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { AppDispatch } from '../../app/store';

const Container = styled.section`
  background-color: white;
  max-width: 300px;
  margin: 3rem auto;
  padding: 2rem;
  text-align: center;
`;

const InputWrapper = styled.label`
  font-weight: bold;
  display: flex;
  flex-direction: column;
  text-align: left;
  margin-bottom: 1rem;

  input {
    padding: 5px;
    font-size: 1.2rem;
  }
`;

const InputRemember = styled.label`
  display: flex;
  gap: 4px;
  align-items: center;
`;

const SubmitBtn = styled.button`
  cursor: pointer;
  width: 100%;
  padding: 8px;
  font-size: 1.1rem;
  font-weight: bold;
  margin-top: 1rem;
  border: none;
  background-color: #00bc77;
  color: #fff;
  text-decoration: underline;

  &:hover {
    background-color: #00b170;
  }

  &:disabled {
    cursor: initial;
    color: rgba(0,0,0,.26);
    background-color: #dcdcdc;
  }
`;

const SignInForm: React.FC = () => {
  const history = useHistory();
  const dispatch: AppDispatch = useAppDispatch();
  const { handleSubmit, register } = useForm();
  const {
    isLoggedIn, isFetching, isError, errorMessage,
  } = useAppSelector((state) => state.user);

  const onSubmit = (data: { email: string; password: string; }) => {
    dispatch(userLogin(data));
  };

  useEffect(() => {
    dispatch(clearState());
  });

  useEffect(() => {
    if (isError) {
      toast.error(errorMessage);
      dispatch(clearState());
    }

    if (isLoggedIn) {
      dispatch(clearState());
      history.push('/profile');
    }
  });

  return (
    <Container>
      <FontAwesomeIcon icon={faUserCircle} size="lg" />
      <h1>Sign In</h1>
      <form onSubmit={handleSubmit(onSubmit)} method="POST">
        <InputWrapper htmlFor="email">
          Email
          <input id="email" type="email" {...register('email')} required />
        </InputWrapper>
        <InputWrapper htmlFor="password">
          Password
          <input id="password" type="password" {...register('password')} required />
        </InputWrapper>
        <InputRemember htmlFor="remember-me">
          <input id="remember-me" type="checkbox" />
          Remember me
        </InputRemember>
        <SubmitBtn type="submit">
          {isFetching ? 'Fetching ...' : 'Sign in'}
        </SubmitBtn>
      </form>
    </Container>
  );
};

export default SignInForm;
