/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserCircle, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { useHistory } from 'react-router';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { AppDispatch } from '../../app/store';
import { resetUserState } from '../../features/User/UserSLice';

const Container = styled.header`
  min-height: 65px;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: center;
  padding: 5px 20px;
`;

const Logo = styled.img`
  max-width: 100%;
  width: 200px;
`;

const StyledLink = styled(Link)`
  font-weight: bold;
  text-decoration: none;
  display: flex;
  align-items: center;
  gap: 4px;
  margin-right: 8px;

  &:hover {
    text-decoration: underline;
    text-decoration-thickness: 2.3px;
  }
`;

const Actions = styled.div`
  display: flex;
  gap: 8px;
`;

const SignOut = styled.div`
  font-weight: bold;
  display: flex;
  align-items: center;
  gap: 4px;
  margin-right: 8px;
  cursor: pointer;

  &:hover {
    text-decoration: underline;
    text-decoration-thickness: 2.3px;
  }
`;

const Header: React.FC = () => {
  const history = useHistory();
  const dispatch: AppDispatch = useAppDispatch();

  const {
    isLoggedIn, userFirstName,
  } = useAppSelector((state) => state.user);

  const onLogOut = () => {
    localStorage.removeItem('token');
    dispatch(resetUserState());
    history.push('/');
  };

  return (
    <Container>
      <Link to="/">
        <Logo src="/assets/argentBankLogo.png" alt="argent-bank logo" />
      </Link>
      {isLoggedIn ? (
        <Actions>
          <StyledLink to="/profile">
            <FontAwesomeIcon icon={faUserCircle} size="1x" />
            { userFirstName }
          </StyledLink>
          <SignOut onClick={onLogOut}>
            <FontAwesomeIcon icon={faSignOutAlt} size="1x" />
            <span>
              Sign Out
            </span>
          </SignOut>
        </Actions>
      ) : (
        <StyledLink to="/sign-in">
          <FontAwesomeIcon icon={faUserCircle} size="1x" />
          <span>
            Sign In
          </span>
        </StyledLink>
      )}
    </Container>
  );
};

export default Header;
