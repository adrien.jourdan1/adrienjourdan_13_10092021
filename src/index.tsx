import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { store } from './app/store';
import './utils/style/global.css';
import 'react-toastify/dist/ReactToastify.css';
import Routing from './route';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ToastContainer autoClose={2000} position="top-right" closeOnClick hideProgressBar />
      <Routing />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);
