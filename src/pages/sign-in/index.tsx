import React from 'react';
import styled from 'styled-components';
import SignInForm from '../../components/sign-in/SignInForm';

const Container = styled.main`
  width: 100%;
  background-color: #12002b;
`;

const SignIn: React.FC = () => (
  <Container>
    <SignInForm />
  </Container>
);

export default SignIn;
