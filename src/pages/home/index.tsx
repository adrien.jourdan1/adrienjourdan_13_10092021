import React from 'react';
import styled from 'styled-components';
import Hero from '../../components/home/Hero';
import Features from '../../components/home/Features';

const Container = styled.main`
  width: 100%;  
`;

const Home: React.FC = () => (
  <Container>
    <Hero />
    <Features />
  </Container>
);

export default Home;
