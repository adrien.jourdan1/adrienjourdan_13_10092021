/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import Loader from 'react-loader-spinner';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { AppDispatch } from '../../app/store';
import { fetchUserBytoken, clearState } from '../../features/User/UserSLice';
import ProfileAccountList from '../../components/profile/profileAccountList';
import ProfileEditModal from '../../components/profile/profileEditModal';

const Container = styled.main`
  width: 100%;
  background-color: #12002b;

  header {
    display: flex;
    flex-direction: column;
    margin-bottom: 2rem;

    h1 {
      color: white;
      text-align: center;
    }
  }
`;

const Button = styled.button`
  border-color: #00bc77;
  background-color: #00bc77;
  color: #fff;
  font-weight: bold;
  padding: 10px;
  margin: 0 auto;
`;

const StyledLoader = styled(Loader)`
  display: flex;
  justify-content: center;
  margin-top: 75px;
`;

const Profile: React.FC = () => {
  const history = useHistory();
  const dispatch: AppDispatch = useAppDispatch();
  const {
    isFetching, isError, isLoggedIn,
  } = useAppSelector((state) => state.user);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    if (isLoggedIn) {
      dispatch(fetchUserBytoken({ token: localStorage.getItem('token') as string }));
    } else {
      dispatch(clearState());
      history.push('/');
    }
  }, [dispatch, history, isLoggedIn]);

  const { userFirstName, userLastName } = useAppSelector((state) => state.user);

  useEffect(() => {
    if (isError) {
      dispatch(clearState());
      history.push('/login');
    }
  }, [dispatch, history, isError]);

  const onClick = () => {
    if (!modalOpen) {
      setModalOpen(true);
    }
  };

  return (
    <Container>
      {isFetching ? (
        <StyledLoader type="Grid" color="#13d18b" height={100} width={100} />
      ) : (
        <div>
          <header>
            <h1>
              Welcome back
              <br />
              {`${userFirstName} ${userLastName}!`}
            </h1>
            <Button type="button" onClick={onClick}>Edit Name</Button>
          </header>
          <ProfileAccountList />
          <ProfileEditModal open={modalOpen} updateModal={setModalOpen} />
        </div>
      )}
    </Container>
  );
};

export default Profile;
