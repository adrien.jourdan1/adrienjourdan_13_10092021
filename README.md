[![OpenClassrooms](https://img.shields.io/badge/-OpenClassrooms-%237451eb)](https://openclassrooms.com/fr/)
[![React](https://img.shields.io/badge/-React-%2361dafb)](https://reactjs.org/)
[![Typescript](https://img.shields.io/badge/-Typescript-%233178c6)](https://www.typescriptlang.org/)
[![html](https://img.shields.io/badge/-Html-%23dd4b25)](https://developer.mozilla.org/en-US/docs/Web/HTML)
[![css](https://img.shields.io/badge/-CSS-%23264de4)](https://developer.mozilla.org/en-US/docs/Web/CSS)
[![Creative Commons](https://img.shields.io/badge/Creative%20Commons-BY--NC--ND-black)](https://creativecommons.org/)

# Welcome to Argent Bank

### ArgentBank is the thirteenth project of the Openclassrooms' Front-End developer path !

### Use an API for a bank user account with React

- Implement a state manager in a React app
- Interact with an API
- Model an API
- Authenticate to an API

# Technologies

### language

- [Typescript](https://www.typescriptlang.org/) v4.1.5

### framework

- [React](https://reactjs.org/) v17.0.2

### dependencies

- [React Router](https://reactrouter.com/) v5.2.0
- [React Redux](https://react-redux.js.org/) v7.2.0
- [Styled Components](https://styled-components.com/) v5.3.0
- [React Loader Spinner](https://www.npmjs.com/package/react-loader-spinner) v4.0.0
- [React Toastify](https://www.npmjs.com/package/react-toastify)

# Getting Started

### Prerequisites

- [NodeJS](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/try/download/community)
- [Yarn](https://yarnpkg.com/)

### Instructions

1. Get the [Backend API project](https://github.com/OpenClassrooms-Student-Center/Project-10-Bank-API)
2. Clone this repository on your computer
3. Open a terminal window in the cloned project
4. Run the following commands:

```
# Install dependencies
yarn

# Start local dev server
yarn start
```

- _Don't forget to run the **Backend API**_
- Your server should now be running at http://locahost:3000

# Author

> Adrien JOURDAN -> check [my gitlab](https://gitlab.com/adrien.jourdan1) and [my linkedIn profile](https://www.linkedin.com/in/adrienjourdan/)

# Contributing

> [Hamza Fahi](https://www.linkedin.com/in/mattindir/) as OpenClassrooms mentor

> [Openclassrooms](https://openclassrooms.com/fr/) as my learning platform
